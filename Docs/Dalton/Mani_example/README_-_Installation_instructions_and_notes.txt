These notes are mostly based on what Richard Thurston told me, and the scripts in this folder are almost all from him -Maniratnam Iyer

Dalton
https://gitlab.com/dalton/dalton
Manual for 2018.2: https://daltonprogram.org/manuals/dalton2018manual.pdf
Useful order of reading in manual: Ch. 4, 24, 5, 6, 11 (for second order hyperpolarizability)
11.2.3: only gives real part of gamma
Useful resources: Dalton forum - http://forum.daltonprogram.org/

Installation:
git clone --recursive https://gitlab.com/dalton/dalton.git
git checkout Dalton2018.2 #can update to latest version
git submodule update
module rm python/2.71
module add python/3.7
module add intel/2016.4.072
module add mpich2/3.2.1
sbatch build_and_test_dalton.sh #edit script first
