#!/bin/bash
# Job name:
#SBATCH --job-name=Build_Dalton
#
# Partition: 
#SBATCH --partition=etna
#
# Wall clock limit: (18 hours as testing takes a long time - 8 hours for me) 
#SBATCH --time=18:00:00
#
# Processors:
#SBATCH --ntasks=2
#
# QoS: 
#SBATCH --qos=normal
#
# Account:
#SBATCH --account=nano
#
# Mail Type: 
#SBATCH --mail-type=all
#
# Mail User: 
#SBATCH --mail-user=mmiyer@lbl.gov
#

## Run Command
cd /global/home/users/maniratnam_iyer/dalton
./setup --mpi
cd build
make
ctest
cd ..
mv build build_2016_intel_openmpi_mkl_python37
