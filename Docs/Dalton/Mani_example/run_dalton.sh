#!/bin/bash
# Job name:
#SBATCH --job-name=Run_Dalton
#
# Partition: 
#SBATCH --partition=etna
#
# Wall clock limit: 
#SBATCH --time=05:00
#
# Processors:
#SBATCH --ntasks=2
#
# QoS: 
#SBATCH --qos=normal
#
# Account:
#SBATCH --account=nano
#
# Mail Type: 
#SBATCH --mail-type=all
#
# Mail User: 
#SBATCH --mail-user=mmiyer@lbl.gov
#

## Run Command
module add mpich2/3.2.1
$HOME/dalton/build_2016_intel_openmpi_mkl_python37/dalton $1 $2
