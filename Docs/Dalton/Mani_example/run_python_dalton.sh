#!/bin/bash
# Job name:
#SBATCH --job-name=Run_Dalton
#
# Partition: 
#SBATCH --partition=lr3
#
# Wall clock limit: 
#SBATCH --time=96:0:0
#
# Processors:
#SBATCH --ntasks=9
#
# QoS: 
#SBATCH --qos=condo_axl
#
# Account:
#SBATCH --account=lr_axl
#
# Mail Type: 
#SBATCH --mail-type=all
#
# Mail User: 
#SBATCH --mail-user=rthurston@lbl.gov
#

## Run Command
cd /global/home/users/rthurston/dalton/build_2018_intel_openmpi_mkl
python -u qchem_nitrobenzene/main_calc.py qchem_nitrobenzene/json_config/config.json
